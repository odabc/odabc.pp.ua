jQuery(function($) {

	//#main-slider
	$(function(){
		$('.carousel').carousel({
			interval: 4000
		});
	});

	//#audio custom buttons
	$('#audio-play').click(function() {
		document.getElementById('peaks-audio').play();
		$('#audio-play').addClass('hide');
		$('#audio-pause').removeClass('hide');
	});

	$('#audio-pause').click(function() {
		document.getElementById('peaks-audio').pause();
		$('#audio-pause').addClass('hide');
		$('#audio-play').removeClass('hide');
	});

	$('#peaks-audio').on('ended', function() {
		$('#audio-pause').addClass('hide');
		$('#audio-play').removeClass('hide');
		$('#peaks-audio').load();
	});

	// clear modal window
	$(document).on("hidden.bs.modal", function (e) {
		$(e.target).removeData("bs.modal").find(".modal-content").empty();
	});

	// center position
	$( '.centered' ).each(function( e ) {
		$(this).css('margin-top',  ($('#main-slider').height() - $(this).height())/2);
	});

	// resize item
	$(window).resize(function(){
		$( '.centered' ).each(function( e ) {
			$(this).css('margin-top',  ($('#main-slider').height() - $(this).height())/2);
		});
	});
});