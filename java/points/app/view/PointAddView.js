Ext.define('PointApp.view.PointAddView', {
    extend: 'Ext.window.Window',
    alias: 'widget.pointAddView',
    autoShow: true,
    layout: 'absolute',
    position: 'center',
    resizable: false,
    closable:  false,
    draggable: true,
    modal: true,

    items: [
        {
            bodyPadding: 10,
            xtype: 'form',
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: 'Название точки',
                    allowBlank: false,
                    blankText: 'Это поле должно быть заполнено'
                },
                {
                    xtype: 'numberfield',
                    name: 'x',
                    fieldLabel: 'Координата X',
                    maxValue: 2147483647,
                    minValue: -2147483648,
                    allowBlank: false,
                    blankText: 'Это поле должно быть заполнено'
                },
                {
                    xtype: 'numberfield',
                    name: 'y',
                    fieldLabel: 'Координата Y',
                    maxValue: 2147483647,
                    minValue: -2147483648,
                    allowBlank: false,
                    blankText: 'Это поле должно быть заполнено'
                }
            ]
        }
    ],

    buttons: [
        {
            text: 'Сохранить',
            action: 'save',
            disabled: true
        },
        {
            text: 'Отменить',
            handler: function () {
                this.up('window').close();
            }
        }
    ]
});
