Ext.define('PointApp.view.PointChartView', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.pointChartView',
    store: 'PointStore',
    frame: true,

    interactions: {
        type: 'panzoom'
    },

    axes: [{
        type: 'numeric',
        position: 'left'
    }, {
        type: 'numeric',
        position: 'bottom'
    }],

    series: [{
        type: 'line',
        xField: 'x',
        yField: 'y',
        style: {
            stroke: "#115fa6",
            fill: null,
            fillOpacity: 0,
            miterLimit: 3,
            lineCap: 'miter',
            lineWidth: 2
        }
    }]
});