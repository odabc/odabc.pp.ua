Ext.define('PointApp.view.PointGridView', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pointGridView',
    store: 'PointStore',
    frame: true,

    columns: [
        {
            flex: 1,
            text: 'ID',
            sortable: true,
            dataIndex: 'id',
            edit: false
        },
        {
            flex: 2,
            text: 'Название',
            sortable: true,
            dataIndex: 'name',
            editor: {
                xtype: 'textfield',
                allowBlank: false,
                blankText: 'Это поле должно быть заполнено'
            }
        },
        {
            flex: 3,
            text: 'Координата X',
            sortable: true,
            dataIndex: 'x',
            editor: {
                xtype: 'numberfield',
                maxValue: 2147483647,
                minValue: -2147483648,
                allowBlank: false,
                blankText: 'Это поле должно быть заполнено'
            }
        },
        {
            flex: 4,
            text: 'Координата Y',
            sortable: true,
            dataIndex: 'y',
            editor: {
                xtype: 'numberfield',
                maxValue: 2147483647,
                minValue: -2147483648,
                allowBlank: false,
                blankText: 'Это поле должно быть заполнено'
            }
        }

    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            saveBtnText: 'Сохранить',
            cancelBtnText: 'Отменить'
        })
    ],
    selType: 'rowmodel',
    dockedItems: [
        {
            xtype: 'toolbar',
            items: [
                {
                    text: 'Добавить',
                    action: 'add',
                    iconCls: 'icon-add'
                },
                '-',
                {
                    text: 'Удалить',
                    action: 'delete',
                    iconCls: 'icon-delete',
                    disabled: true
                }
            ]
        }
    ]
});