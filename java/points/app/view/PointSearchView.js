Ext.define('PointApp.view.PointSearchView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.pointSearchView',
    bodyPadding: 10,
    frame: true,

    items: [{
        xtype: 'textfield',
        name: 'search',
        maxLength: 100
    }]
});