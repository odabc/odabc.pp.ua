Ext.define('PointApp.controller.PointSearchController', {
    extend: 'Ext.app.Controller',

    refs: [
        {selector: 'pointSearchView', ref: 'pointSearchView'}
    ],

    init: function () {
        this.control({
            'pointSearchView textfield[name="search"]': {
                change: this.onChangeText
            }
        })
    },

    onChangeText: function () {
        Ext.getStore('PointStore').load({
            params: {
                search: this.getPointSearchView().getValues()
            }
        });
    }
});