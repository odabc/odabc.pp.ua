Ext.define('PointApp.controller.PointAddController', {
    extend: 'Ext.app.Controller',

    refs: [
        {selector: 'pointAddView', ref: 'pointAddView'},

        {selector: 'pointAddView textfield[name=name] ', ref: 'pointAddName'},
        {selector: 'pointAddView numberfield[name=x]', ref: 'pointAddX'},
        {selector: 'pointAddView numberfield[name=y]', ref: 'pointAddY'},
        {selector: 'pointAddView button[action=save]', ref: 'pointAddSave'}
    ],

    init: function () {
        this.control({
            'pointAddView  button[action=save]': {
                click: this.onSavePoint
            },
            'pointAddView  textfield[name="name"]': {
                change: this.onValidation
            },
            'pointAddView  numberfield[name="x"]': {
                change: this.onValidation
            },
            'pointAddView  numberfield[name="y"]': {
                change: this.onValidation
            }
        });
    },

    onSavePoint: function () {
        var hangPoint = Ext.create('PointApp.model.Point');
        hangPoint.set(this.getPointAddView().down('form').getValues());
        hangPoint.set('id', -1);

        hangPoint.save({
            success: function (record, operation) {
                var store = Ext.getStore('PointStore');
                store.add(record);
                store.sync();
            },
            failure: function (record, operation) {
                Ext.MessageBox.show({
                    title: 'Ошибка!',
                    msg: 'Что-то не так пошло!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        });

        this.getPointAddView().close();
    },

    onValidation: function () {
        if (this.getPointAddName().validate() && this.getPointAddX().validate() && this.getPointAddY().validate()) {
            this.getPointAddSave().enable();
        } else {
            this.getPointAddSave().disable();
        }
    }
});