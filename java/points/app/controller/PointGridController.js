Ext.define('PointApp.controller.PointGridController', {
    extend: 'Ext.app.Controller',

    refs: [
        {selector: 'pointGridView', ref: 'pointGridView'},

        {selector: 'pointGridView button[action="delete"]', ref: 'pointGridDelete'}
    ],

    init: function () {
        this.control({
            'pointGridView  button[action=add]': {
                click: this.onAddPoint
            },
            'pointGridView  button[action=delete]': {
                click: this.onDelPoint
            },
            'pointGridView': {
                cellclick: this.onLineGrid
            }
        });
    },

    onAddPoint: function () {
        Ext.create('PointApp.view.PointAddView');
    },

    onDelPoint: function () {
        var delPoint = this.getPointGridView().getSelectionModel().getSelection()[0];
        var pointStore = this.getPointGridView().store;

        pointStore.remove(delPoint);
        pointStore.commitChanges();
        this.getPointGridDelete().disable();
    },

    onLineGrid: function () {
        this.getPointGridDelete().enable();
    }
});